# YAML-cut

Cut a Swagger/OpenAPI spec file by separating its paths into smaller files.

# Install

```
git clone git@framagit.org:Nautigsam/yaml-cut.git
cd yaml-cut
npm install
```

# Usage

Check script's help:

```
$ ./yamlcut -h
```

You can specify the working directory with `--workdir` option. Specify the file you want to work with:

```
$ ./yamlcut -f pet-store.yaml # By default the workdir is "."
```

Give each path a reference name:

```
$ ./yamlcut -f pet-store.yaml 
✔ Reference name for /pets: … Pets
✔ Reference name for /pets/{id}: … Pet
```

Et voilà! Your file is now smaller. The paths are in `paths` sub-directory of the working directory.
