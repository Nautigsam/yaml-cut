'use strict'

const path = require('path')

const fse = require('fs-extra')
const prompts = require('prompts')
const YAML = require('yaml')
const Map = require('yaml/map')
const Pair = require('yaml/pair')
const yargs = require('yargs')

const PATHS_DIRNAME = 'paths'

const PARSE_OPTIONS = {
	...YAML.defaultOptions,
	keepCstNodes: false,
	keepNodeTypes: false
}

async function readYaml(path) {
	const file = await fse.readFile(path, 'utf8')
	return YAML.parseDocument(file, PARSE_OPTIONS)
}

async function main(argv) {
	const ROUTE_REGEX = RegExp(`^${argv.pathPrefix}/([^/]+)/?`)
	let WORKDIR
	if (path.isAbsolute(argv.workdir)) {
		WORKDIR = argv.workdir
	} else {
		WORKDIR = path.resolve(process.cwd(), argv.workdir)
	}
	const PATHS_DIR = path.join(WORKDIR, PATHS_DIRNAME)
	const SRC_FILE_PATH = path.join(WORKDIR, argv.file)

	await fse.ensureDir(PATHS_DIR)
	const srcDoc = await readYaml(SRC_FILE_PATH)

	const {value: paths} = srcDoc.contents.items.find(item => item.key.value === 'paths')
	const routes = paths.items
		.filter(item => item.value.items[0].key.value !== '$ref')
		.map(item => ({
			item,
			path: item.key.value,
			content: item.value
		}))
	for await (const route of routes) {
		const matches = route.path.match(ROUTE_REGEX)
		if (!matches) {
			continue
		}
		const section = matches[1]
		const sectionFilename = `${section}.yaml`
		const sectionFile = path.join(PATHS_DIR, sectionFilename)
		const sectionFileRelPath = path.join(path.relative(path.dirname(SRC_FILE_PATH), path.dirname(sectionFile)), sectionFilename)
		let doc
		if (fse.pathExistsSync(sectionFile)) {
			doc = await readYaml(sectionFile)
		} else {
			doc = new YAML.Document()
			doc.contents  = YAML.createNode({})
		}
		const response = await prompts({
			type: 'text',
			name: 'routeName',
			message: `Reference name for ${route.path}:`,
			validate: value => value.length > 0
		}, {
			onCancel() {
				process.exit(1)
			}
		})
		const ref = `${sectionFileRelPath}#/${response.routeName}`
		route.item.value = YAML.createNode({'$ref': ref})
		const newNode = new Pair(response.routeName, route.content)
		await fse.writeFile(SRC_FILE_PATH, YAML.stringify(srcDoc.contents))
		doc.contents.items.push(newNode)
		await fse.writeFile(sectionFile, YAML.stringify(doc.contents))
	}
}

try {
	const argv = yargs
		.option('file', {
			alias: 'f',
			type: 'string',
			default: 'swagger.yaml',
			describe: 'The file to cut'
		})
		.option('workdir', {
			alias: 'd',
			type: 'string',
			default: '.',
			describe: 'The working directory'
		})
		.option('path-prefix', {
			alias: 'p',
			type: 'string',
			default: '',
			describe: 'A prefix for API paths'
		})
		.alias('help', 'h')
		.argv
	main(argv)
} catch (err) {
	console.error(err)
}
